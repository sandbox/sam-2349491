<?php

$plugin = array(
  'title' => t('Variable Access'),
  'description' => t('Access rules based on variables.'),
  'callback' => 'variable_access_access_callback',
  'settings form' => 'variable_access_settings_from',
  'summary' => 'variable_access_summary',
  'default' => array(
    'variable' => '_none_',
    'fail_value' => '0',
  ),
);

/**
 * A summary of the access callback.
 */
function variable_access_summary($conf, $context) {
  return t('Show when @variable is not equal to "@value".', array('@variable' => $conf['variable'], '@value' => $conf['fail_value']));
}

/**
 * Settings form to set which variables and values trigger the access check.
 */
function variable_access_settings_from($form, &$form_state, $conf) {
  $variables = array_keys($GLOBALS['conf']);
  $variables = array_combine($variables, $variables);
  $form['settings']['variable'] = array(
    '#title' => t('Variable'),
    '#description' => t('Select the variable which will be the basis of the access check.'),
    '#type' => 'select',
    '#options' => $variables,
    '#default_value' => $conf['variable'],
    '#empty_value' => '_none_',
    '#required' => TRUE,
  );
  $form['settings']['fail_value'] = array(
    '#title' => t('Fail Value'),
    '#description' => t('Enter the value which the above variable will be set to, to result in a failed access check. Defaults to "0".'),
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $conf['fail_value'],
  );
  return $form;
}

/**
 * Access callback which checks the values of the given variables.
 */
function variable_access_access_callback($conf, $context) {
  return variable_get($conf['variable'], "0") != $conf['fail_value'];
}
